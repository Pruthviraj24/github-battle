import {Link} from "react-router-dom"
import {TiDelete} from "react-icons/ti"
import "./index.css"


const Players = (props) =>{
    const {
        playerOneInput,
        playerTwoInput,
        updatePlayerOne,
        updatePlayerTwo,
        getSecondPlayer,
        getFirstPlayer,
        playerTwo,
        playersOne,
        deleteSecondPlayer,
        deleteFirstPlayer,
        lightMode
    } = props


    // Updating players input value

    const updateFirstPlayer = (event)=>{
        updatePlayerOne(event.target.value) 
    }

    const updateSecondPlayer = (event)=>{
        updatePlayerTwo(event.target.value)
    }

    // Making call to get players data
    const getPlayerOne = (event)=>{
        getFirstPlayer(event.target.value)
    }

    const getPlayerTwo = (event)=>{
        getSecondPlayer(event.target.value)
    }

    // Deleting players

    const removePlayerTwo = ()=>{
        deleteSecondPlayer()
    }

    const removePlayerOne = ()=>{
        deleteFirstPlayer()
    }



    return (
        <section className="player-inputs-container">
            <h2 className="battle-inputs-header">Players</h2>
            <div className="battle-inputs-container">
            
         {/* First player input and card    */}

            <div className="player-one-card">
               {!playersOne.login && <div>
                    <label htmlFor="player-one">Player One</label>
                    <div className="input-button-container">
                        <input id="player-one" placeholder="github Username" className={`player-input ${lightMode ? "player-input-light" : "player-input-dark"}`} value={playerOneInput} type="text" onChange={updateFirstPlayer} />
                        <button className="submit-button" type="button" onClick={getPlayerOne} value={playerOneInput} disabled={!playerOneInput}>SUBMIT</button>
                    </div>
                </div>}
                {playersOne.login && 
                <>
                    <p>Player One</p>
                    <div className={`player-card-container ${lightMode ? "player-card-container-light" : "player-card-container-dark"}`}>
                        <div className="avatar-name-container">
                            <img className="player-avatar" src={playersOne !== undefined && playersOne.avatarUrl} alt={`${playersOne.login} avatar`}/>
                            <p>{playersOne!== undefined && playersOne.login}</p>
                        </div>
                        <button className="remove-player-button" onClick={removePlayerOne} type="button"><TiDelete/></button>
                    </div>
                </>
                }
            </div>

        {/* Second player input and card  */}

             <div className="player-two-card">
                {!playerTwo.login &&  <div className="">
                    <label htmlFor="palyer-two" className="player-label">Player Two</label>
                    <div className="input-button-container">
                        <input id="palyer-two" placeholder="github Username"  className={`player-input ${lightMode ? "player-input-light" : "player-input-dark"}`} value={playerTwoInput} type="text"  onChange={updateSecondPlayer}/>
                        <button className="submit-button" type="button" onClick={getPlayerTwo} value={playerTwoInput} disabled={!playerTwoInput}>SUBMIT</button>
                    </div>
                    </div>}
                    {playerTwo.login && 
                    <>
                        <p>Player Two</p>
                        <div className={`player-card-container ${lightMode ? "player-card-container-light" : "player-card-container-dark"}`}> 
                            <div className="avatar-name-container">
                                <img className="player-avatar" src={playerTwo!== undefined ? playerTwo.avatarUrl : ""} alt={playerTwo.login}/>
                                <p>{playerTwo!== undefined && playerTwo.login}</p>
                            </div>
                            <button className="remove-player-button" onClick={removePlayerTwo} type="button"><TiDelete/></button>
                        </div>
                    </>
                }
            </div>
        </div>
        
        {/* Battle button will be displayd only when both players are submitted */}

        {playersOne.login && playerTwo.login && 
            <Link to={`/battle/results/${playersOne.login}/${playerTwo.login}`} >
                <button className="battle-button" type="button">BATTLE</button>
            </Link>
        }
    </section>
    )
}


export default Players
