import { Component } from "react";
import {HiUsers} from "react-icons/hi"
import {FaFighterJet} from "react-icons/fa"
import {FaTrophy} from "react-icons/fa"
import Players from "../Players/Players";

import "./index.css"

class Battle extends Component{
    state = {playersOne:{},playerTwo:{},firstPlayer:"",secondPlayer:""}



    // Fetching player data 
    
    getPlayer = async (player)=>{
        const url = `https://api.github.com/users/${player}`

        const response = await fetch(url)
      
        if(response.ok){
            const responseData = await response.json()
            const updatedData = {
                avatarUrl:responseData.avatar_url,
                login:responseData.login,
                id:responseData.id,
            }
            return updatedData
        }else{
            console.log(response);
            const updatedData = {
                avatarUrl:"",
                login:player,
                id:"",
            }
            return updatedData
        }
    }

    
    // Making fecth call to get firstplayer

    getFirstPlayer = async (player)=>{
        const data = await this.getPlayer(player)
        this.setState({
            playersOne:data,
            firstPlayer:""
        })
    }

    // Making fecth call to get secondplayer

    getSecondPlayer = async (player)=>{
        const data = await this.getPlayer(player)
        this.setState({
            playerTwo:data,
            secondPlayer:""
        })
    }

    // Updating player one 

    updatePlayerOne = (firstPlayerValue)=>{
        this.setState({
            firstPlayer:firstPlayerValue
        })
    }

    // Updating Player two

    updatePlayerTwo = (secondPlayerValue)=>{
        this.setState({
            secondPlayer:secondPlayerValue
        })
    }


    // Deleting player data onClick delete button 

    deleteFirstPlayer = ()=>{
        this.setState({
            playersOne:{}
        })
    }

    deleteSecondPlayer = ()=>{
        this.setState({
            playerTwo:{}
        })
    }


    render(){
        const {playersOne,playerTwo,firstPlayer,secondPlayer} = this.state
        const {lightMode} = this.props

        return(
            <div className={`battle-section-container ${lightMode ? "light-mode-battle": "dark-mode-battle"}`}>
                <h1 className="instruction-header">Instruction</h1>
                <ul className="instruction-list">
                    <li>
                        <h2>Enter two Github Users</h2>
                        <HiUsers className={`icons ${lightMode ? "users-icon-light" : "users-icon-dark" }`}/>
                    </li>
                    <li>
                        <h2>Battle</h2>
                        <FaFighterJet className={`icons ${lightMode ? "flight-icon-light" : "flight-icon-dark"}`}/>
                    </li>
                    <li>
                        <h2>See the winner</h2>
                        <FaTrophy className={`icons ${lightMode ? "trophy-icon-light" : "trophy-icon-dark"}`}/>
                    </li>
                </ul>
                <Players 
                    playerTwo={playerTwo} 
                    playersOne={playersOne} 
                    getFirstPlayer={this.getFirstPlayer} 
                    getSecondPlayer={this.getSecondPlayer}  
                    playerOneInput={firstPlayer} 
                    playerTwoInput={secondPlayer} 
                    updatePlayerOne={this.updatePlayerOne} 
                    updatePlayerTwo={this.updatePlayerTwo}
                    deleteSecondPlayer={this.deleteSecondPlayer}
                    deleteFirstPlayer={this.deleteFirstPlayer}
                    lightMode = {lightMode}
                    />

            </div>
        )
    }

}

export default  Battle