import { Component } from "react";
import {ThreeDots} from "react-loader-spinner" 
import {FaUserAlt} from "react-icons/fa"
import {BsFillStarFill} from "react-icons/bs"
import {BiGitBranch} from "react-icons/bi"
import {MdWarning} from "react-icons/md"
import "./index.css"



const apiStatusConstants = {
    initial: "INITIAL",
    success: "SUCCESS",
    failure: "FAILURE",
    inProgress: "IN_PROGRESS",
  };


const languages = {
    all:"All",
    javascript:"JavaScript",
    ruby:"Ruby",
    java:"Java",
    css:"CSS",
    python:"Python"
}



class Popular extends Component{
    state = {popularData:[],apiStatus:apiStatusConstants.initial}

    componentDidMount(){
        this.getData()
    }


    // Fetching Popular Data

    getData = async (value=languages.all)=>{

        const url = `https://api.github.com/search/repositories?q=stars:%3E1+language:${value}&sort=stars&order=desc&type=Repositories`

        this.setState({apiStatus:apiStatusConstants.inProgress})

        const resopnse = await fetch(url)

        if(resopnse.ok){
            const resopnseData = await  resopnse.json()
        
            const updatedData = resopnseData.items.map(eachItem=>({
                id:eachItem.id,
                avatarUrl:eachItem.owner.avatar_url,
                name:eachItem.owner.login,
                htmlUrl:eachItem.owner.html_url,
                stars:eachItem.stargazers_count,
                openIssues:eachItem.open_issues,
                forks:eachItem.forks
            }))

            this.setState({
                popularData:updatedData,
                apiStatus:apiStatusConstants.success,
                activeSection:value
            })
        }else{
            this.setState({
                apiStatus:apiStatusConstants.failure
            })
        }

    }

    // Rendering Loading view 

    renderLoader = ()=>{
        const {lightMode} = this.props

        return(
            <div className={lightMode ? "wraper-light" : "wraper-dark"}>
            <h1 >Fetching Repos</h1>
           <ThreeDots
                    content="Featching Repos"
                    height="80" 
                    width="80" 
                    radius="9"
                    color="#4fa94d" 
                    ariaLabel="three-dots-loading"
                    wrapperStyle={{}}
                    wrapperClassName=""
                    visible={true}
                    
                    />
            </div>
        )
    }


    // Rendering Success view

    renderPopularData = ()=>{
        const {popularData} = this.state
        const {lightMode} = this.props

        return(
            <ul className={`popularList-container  ${lightMode ? "popularList-light-container" : "popularList-dark-container" }`}>
                {
                    popularData.map((eachData,index)=>{
                        return (
                            <li className={`popularList  ${lightMode ? "popularList-light" : "popularList-dark" }`} key={eachData.id}>
                                <div className="popular-image-header">
                                    <h2 className="ranks-header">{`# ${index+1}`}</h2>
                                    <img className="avatar-image" src={eachData.avatarUrl} alt={eachData.name}/>
                                    <h4 className="lang-header">{eachData.name}</h4>
                                </div>
                                <div className="popular-list-footer">
                                    <p><FaUserAlt className="footer-icons profile-icon"/>  {eachData.name}</p>
                                    <p><BsFillStarFill className="footer-icons star-icon"/> {eachData.stars.toLocaleString("en-US")} stars</p>
                                    <p><BiGitBranch className="footer-icons git-icon"/> {eachData.forks.toLocaleString("en-US")} forks</p>
                                    <p><MdWarning className="footer-icons warning-icon"/> {eachData.openIssues.toLocaleString("en-US")} open issues</p>
                                </div>
                            </li>
                        )
                    })
                
                }
            </ul>
        )
    }

    // Rendering Failure view 

    renderFailureView = ()=>{
        const {lightMode} = this.props
        return(
            <div className={`fail-container ${lightMode ? "fail-container-light" : "fail-container-dark"}`}>
                <h1>Fail to fetch repos</h1>
            </div>
        )
    }


    renderData = ()=>{
        const {apiStatus} = this.state
 
        // Switching ui based on api fetching status

        switch(apiStatus){
            case apiStatusConstants.inProgress:
                return this.renderLoader()
            case apiStatusConstants.success:
                return this.renderPopularData()
            case apiStatusConstants.failure:
                return this.renderFailureView()
            default:
                return null
        }

    }



    render(){
        const {activeSection} = this.state
        const {lightMode} = this.props
        

        return(
            <section className={`${lightMode ? "popularList-light-container" : "popularList-dark-container"}`}>
                <div className="buttons-container">
                    <button className={activeSection === "All" ? "popular-nav-button-active" : "popular-nav-button-inactive"}  value={languages.all} type="button" onClick={()=>this.getData(languages.all)}>All</button>
                    <button className={activeSection === "JavaScript" ? "popular-nav-button-active" : "popular-nav-button-inactive"} value={languages.javascript} type="button" onClick={()=>this.getData(languages.javascript)}>JavaScript</button>
                    <button className={activeSection === "Ruby" ? "popular-nav-button-active" : "popular-nav-button-inactive"} value={languages.ruby} type="button" onClick={()=>this.getData(languages.ruby)}>Ruby</button>
                    <button className={activeSection === "Java" ? "popular-nav-button-active" : "popular-nav-button-inactive"} value={languages.java} type="button" onClick={()=>this.getData(languages.java)}>Java</button>
                    <button className={activeSection === "CSS" ? "popular-nav-button-active" : "popular-nav-button-inactive"} value={languages.css} type="button" onClick={()=>this.getData(languages.css)}>CSS</button>
                    <button className={activeSection === "Pyton" ? "popular-nav-button-active" : "popular-nav-button-inactive"} value={languages.python} type="button" onClick={()=>this.getData(languages.python)}>Python</button>
                </div>
                {this.renderData()}
            </section>
        )
    }
}


export default Popular