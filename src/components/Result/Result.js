import { Component} from 'react'
import {Link} from "react-router-dom"
import {FaUserAlt,FaUsers} from "react-icons/fa"
import {HiUsers} from "react-icons/hi"
import {BsCodeSlash,BsBriefcaseFill} from "react-icons/bs"
import {ImLocation} from "react-icons/im"
import {ThreeDots} from "react-loader-spinner"
import "./index.css"


const apiStatusConstants = {
  initial: "INITIAL",
  success: "SUCCESS",
  failure: "FAILURE",
  inProgress: "IN_PROGRESS",
};



class Result extends Component {
    state = {firstPlayerResult:{},secondPlayerResult:{},apiStatus:apiStatusConstants.initial}

    
    componentDidMount(){
        this.getBattleData()
    }



    // Getting players data 

    getBattleData = async ()=>{
      const {playerOne,playerTwo} = this.props.pathname.match.params
    
      this.setState({
        apiStatus:apiStatusConstants.inProgress
      })

      const playerOneurl = `https://api.github.com/users/${playerOne}`
      const playerTwoUrl = `https://api.github.com/users/${playerTwo}`

      const playerOneResponse = await fetch(playerOneurl)

      const playerTwoResponse = await fetch(playerTwoUrl)

      if(playerOneResponse.ok && playerTwoResponse.ok){
        const updatePlayer = (playerResponseData)=> ({
          avatarUrl:playerResponseData.avatar_url,
          followers:playerResponseData.followers,
          following:playerResponseData.following,
          name:playerResponseData.name,
          repos:playerResponseData.public_repos,
          login:playerResponseData.login,
          location:playerResponseData.location,
          company:playerOneResponseData.company
        })

          const playerOneResponseData = await playerOneResponse.json()
          const playerTwoResponseData = await playerTwoResponse.json()
   
          const updatedPlayerOne = updatePlayer(playerOneResponseData)
          const updatedPlayerTwo = updatePlayer(playerTwoResponseData)

          this.setState({
            firstPlayerResult:updatedPlayerOne,
            secondPlayerResult:updatedPlayerTwo,
            apiStatus:apiStatusConstants.success
          })
      }else{
          this.setState({
            apiStatus:apiStatusConstants.failure
          })
      }

    }


    // Rendering failure view

    rednerFaliureView = ()=>{
        const {lightMode} = this.props
      return(
        <div className={lightMode ? "fail-container-light" : "fail-container-dark"}>
            <h1 className='fail-header'>Fail to fetch user</h1>
        </div>
      )
    }


    // rendering page loading view

    renderProgressView = ()=>{
      const {lightMode} = this.props
      return(
        <div className={`${lightMode ? "loading-results-light" : "loading-results-dark"}`}>
          <h2>Battling</h2>
              <ThreeDots
              height="80" 
              width="80" 
              radius="9"
              color="#4fa94d" 
              ariaLabel="three-dots-loading"
              wrapperStyle={{}}
              wrapperClassName=""
              visible={true}
              />
        </div>

      )
    }


    // rendering result

    renderResult = ()=>{
      const {firstPlayerResult,secondPlayerResult} = this.state
      const {lightMode} = this.props


      let playerOneResult = "Tie"
      let playerTwoResult = "Tie"
      let playerOneResultData = {...firstPlayerResult}
      let playerTwoResultData = {...secondPlayerResult}
  
      // Calculating score based on followers

      const playerOneScore = firstPlayerResult.followers ? firstPlayerResult.followers * 3: 0
      const playerTwoScore = secondPlayerResult.followers ?   secondPlayerResult.followers *3 : 0
  
      // Updating player result based on score

      if(playerOneScore > playerTwoScore){
        playerOneResult="Winner"
        playerTwoResult = "Looser"
      }else if(playerTwoScore > playerOneScore){
          playerTwoResult = "Winner"
          playerOneResult = "Looser"
      }

      return( 
      <section className={`${lightMode ? "light-result-page" : "dark-result-page"}`}>
        <div className='results-container'>

            {/* First player result card */}

            <div className={`player-data-container ${lightMode ? "light-result-card" : "dark-result-card"}`}>
                <div className='result-card-header'>
                    <h2 >{playerOneResult}</h2>
                    <img className='result-player-avatar' src={playerOneResultData.avatarUrl} alt=""/>
                    <p>Score {playerOneScore}</p>
                    <h5 className='player-name-header'>{playerOneResultData.login}</h5>
                </div>
                  <div className='results-footer-container'>
                    <div>
                        <FaUserAlt className='footer-icons porfile-icon'/>  
                        <p>{playerOneResultData.name}</p>
                    </div>
                    {playerOneResultData.location && <div>
                      <ImLocation className='footer-icons location-icon'/> 
                      <p>{playerOneResultData.location}</p>
                    </div>}
                    {playerOneResultData.company &&<div>
                        <BsBriefcaseFill className='footer-icons company-icon'/> 
                        <p>{playerOneResultData.company}</p>
                    </div>}
                    <div>
                        <FaUsers className='footer-icons followers-icon'/> 
                        <p>{playerOneResultData.followers}  Followers</p>
                    </div>
                    <div>
                        <HiUsers className='footer-icons following-icon'/> 
                        <p>{playerOneResultData.following}  Following</p>
                      </div>
                    <div>
                        <BsCodeSlash className='footer-icons code-icon'/> 
                        <p>{playerOneResultData.repos}  Repositories</p>
                    </div>
                </div>
              </div>
              
            {/* Second player result card */}

            <div  className={`player-data-container ${lightMode ? "light-result-card" : "dark-result-card"}`}>
                <div className='result-card-header'>
                    <h2>{playerTwoResult}</h2>
                    <img className='result-player-avatar' src={playerTwoResultData.avatarUrl} alt=""/>
                    <p>Score {playerTwoScore}</p>
                    <h5  className='player-name-header'>{playerTwoResultData.login}</h5>
                </div>
                <div className='results-footer-container'>
                    <div>
                        <FaUserAlt className='footer-icons porfile-icon'/>
                        <p>{playerTwoResultData.name}</p>
                    </div>
                    {playerTwoResultData.location && <div>
                        <ImLocation className='footer-icons location-icon'/> 
                        <p>{playerTwoResultData.location}</p>
                    </div>}
                    {playerTwoResultData.company && <div>
                        <BsBriefcaseFill className='footer-icons company-icon'/>
                        <p>{playerTwoResultData.company}</p>
                     </div>}
                    <div>
                        <FaUsers className='footer-icons followers-icon'/> 
                        <p>{playerTwoResultData.followers} Followers</p>
                        </div>
                    <div>
                        <HiUsers className='footer-icons following-icon'/> 
                        <p>{playerTwoResultData.following} Following</p>
                    </div>
                    <div>
                        <BsCodeSlash className='footer-icons code-icon'/> 
                        <p>{playerTwoResultData.repos} Repositories</p>
                      </div>
                </div>
            </div>
            
        </div>
            <Link to="/battle">
              <button className='reset-button' type="button">RESET</button>
            </Link>
      </section>)
  }


    // Switching UI based on api response

    renderResultComponent = ()=>{
      const {apiStatus} = this.state

      switch(apiStatus){
        case apiStatusConstants.success:
          return this.renderResult()
        case apiStatusConstants.failure:
          return this.rednerFaliureView()
        case apiStatusConstants.inProgress:
          return this.renderProgressView()
        default:
          return null
      }
    }



  render() {

    return (
      <>
        {this.renderResultComponent()}
      </>)
  }
}

export default Result