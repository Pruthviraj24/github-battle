import {Link} from "react-router-dom"
import "./index.css"


const NotFound = (props)=>{
    const {lightMode} =props
    const imgUrl = lightMode ? "https://img.freepik.com/free-vector/error-404-concept-landing-page_52683-20650.jpg?w=1380&t=st=1664953920~exp=1664954520~hmac=d5a50c730fc33b2486ee3ba6767a33846540686d9495b7809b8c5cc3f566549e"
                            : "https://img.freepik.com/free-vector/404-error-background_23-2148071362.jpg?w=900&t=st=1664954200~exp=1664954800~hmac=3a5e508e8745375a75317cef87ed0c5e65241f165ac574176ffaeee74ddae288"

    return(
        <div className={`not-found-page ${lightMode ? "not-found-light" : "not-found-dark"}`}>
            <img className="not-found-image" src={imgUrl} alt="not-found"/>
            <Link to="/">
                <button className="go-to-home-button" type="button">Go to Home</button>
            </Link>
        </div>
    )
}

export default NotFound