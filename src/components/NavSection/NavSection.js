import {Link,withRouter} from "react-router-dom"
import {MdModeNight,MdOutlineLightMode} from "react-icons/md"
import "./index.css"
import { Component } from "react"



class NavSection extends Component {

    updateMode = ()=>{
        const {modeUpdater} = this.props  // Updating display mode in app.js component
        modeUpdater()
    }


    render(){
        const {pathname} = this.props.location  // getting pathname from props//
        const {lightMode} = this.props  

        return(
            <div className={`nav-bar ${lightMode ? "light-mode-nav" : "dark-mode-nav"}`}>
                <div className="nav-links-container">
                    <Link to="/" className={pathname !== "/" ? "active" : "inactive"}>
                        <p>Popular</p>
                    </Link>
                    <Link to="/battle" className={!pathname.includes("/battle") ? "active" : "inactive"}>
                        <p>Battle</p>
                    </Link>
                </div>
                <div>
                {!lightMode  &&  <button className="dark-mode" onClick={this.updateMode} value="light-mode" type="button">
                        <MdModeNight/>
                    </button>}
                {lightMode &&   <button className="light-mode" value="dark-mode" onClick={this.updateMode} type="button">
                        <MdOutlineLightMode/>
                    </button>}
                </div>
            </div>
        )
    }
  
}

export default withRouter(NavSection)