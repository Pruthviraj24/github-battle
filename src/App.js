import {BrowserRouter ,Route, Switch} from 'react-router-dom'
import Battle from "./components/Battle/Battle.js";
import Popular from "./components/Popular/Popular.js";
import NavSection from './components/NavSection/NavSection';
import NotFound from './components/NotFound/NotFound.js';
import Result from './components/Result/Result';

import React, { Component } from 'react'

class App extends Component {
  state = {lightMode:true}


  // Maintaining the display mode state
  
  updateMode = ()=>{
      this.setState(prevState=>({
          lightMode:!prevState.lightMode
      }))
  }


  render() {
    const {lightMode} = this.state

    return(
      <BrowserRouter>
        <NavSection modeUpdater = {this.updateMode} lightMode = {lightMode}/>
        <Switch>
          <Route  path="/" exact render={() => (<Popular lightMode = {lightMode} />)}/>
          <Route  path="/battle" exact render={() => (<Battle lightMode = {lightMode} />)}/>
          <Route  path="/battle/results/:playerOne/:playerTwo" exact render={(pathname) => (<Result lightMode = {lightMode} pathname={pathname}/>)}/>
          <Route  render={() => (<NotFound lightMode = {lightMode} />)}/>
        </Switch>
      </BrowserRouter>
    )
  }
}

export default App

